package lab01a;

/**
 * Główna klasa aplikacji
 * @author Kamil Krupa
 * @version 1.0
 * @since JDK 1.8
 */
public class Start {

    /**
     * Początkowa metoda od któej aplikacja zacznie swoje działanie
     * @param args tablica parametrów wiersza poleceń
     */
    public static void main(String[] args) {
        Aplikacja aplikacja = new Aplikacja();
        aplikacja.pobierzNumer();
        aplikacja.wyswietlDane();
    }
}
