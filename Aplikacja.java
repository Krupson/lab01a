package lab01a;

import java.util.Scanner;

/**
 * Klasa realizująca logikę biznesową aplikacji
 * @author Kamil Krupa
 * @version 1.0
 * @since JDK 1.8
 */
public class Aplikacja {
    /**
     * Pole przechowujące numer PESEL
     */
    private String PESEL = null;

    /**
     * Konstruktor klasy
     */
    public Aplikacja() {
    }

    /**
     * Metoda pobierająca od użytkownika numer PESEL
     * Pobiera PESEL ze standardowego wejścia, a następnie zapisuje do pola o nazwie {@link Aplikacja#PESEL}
     */
    public void pobierzNumer() {
        System.out.print("Podaj numer PESEL: ");
        Scanner scanner = new Scanner(System.in);
        PESEL = scanner.nextLine();
    }

    /**
     * Metoda wyświetlająca dane na podstawie podanego PESELu
     * Sprawdza poprawność danych, a następnie wyświetla stosowną informację
     */
    public void wyswietlDane() {
        if(PESEL == null) {
            System.out.println("Nie podano numeru PESEL!");
            return;
        }
        if(!Kalkulator.sprawdzPoprawnosc(PESEL)) {
            System.out.println("Podany PESEL nie jest poprawny");
            return;
        }

        Kalkulator.wyswietlDateUrodzenia(PESEL);
        Kalkulator.wyswietlPlec(PESEL);
    }
}
