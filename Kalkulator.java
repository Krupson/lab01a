package lab01a;

/**
 * Klasa odpowiedzialna za obliczenia związane z numerem PESEL
 * @author Kamil Krupa
 * @version 1.0
 * @since JDK 1.8
 */
public class Kalkulator {
    /**
     * Tablica płci
     */
    private static final char[] PLCIE = {'K', 'M'};
    /**
     * Wagi dla poszczególnych cyfr numeru PESEL
     */
    private static final int[] WAGI = {9, 7, 3, 1, 9, 7, 3, 1, 9, 7};

    /**
     * Prywatny konstruktor, aby nie pozwolić na instancjonowanie klasy
     */
    private Kalkulator() {

    }

    /**
     * Sprawdza poprawność numeru PESEL
     * @param pesel numer PESEL
     * @return true, jeżeli PESEL jest poprawny, false jeżeli nie
     */
    public static boolean sprawdzPoprawnosc(String pesel) {
        if(pesel.length() != 11) {
            return false;
        }
        int sumaKontrolna = 0;
        for(int x = 0; x < WAGI.length; x++) {
            sumaKontrolna += (pesel.charAt(x) - 48) * WAGI[x];
        }
        sumaKontrolna %= 10;

        return sumaKontrolna == (pesel.charAt(10) - 48);
    }

    /**
     * Wyświetla datę urodzenia na podstawie numeru PESEL
     * @param pesel numer PESEL
     */
    public static void wyswietlDateUrodzenia(String pesel) {
        if(!sprawdzPoprawnosc(pesel)) {
            System.out.println("Podany PESEL nie jest poprawny");
            return;
        }

        int rok = (pesel.charAt(0) - 48) * 10;
        rok += pesel.charAt(1) - 48;

        int stulecie = pesel.charAt(2) - 48;
        switch(stulecie) {
            case 8:
            case 9:
                rok += 1800;
                break;
            case 0:
            case 1:
                rok += 1900;
                break;
            case 2:
            case 3:
                rok += 2000;
                break;
            case 4:
            case 5:
                rok += 2100;
                break;
            case 6:
            case 7:
                rok += 2200;
                break;
        }

        int miesiac = (stulecie % 2) * 10;
        miesiac += pesel.charAt(3) - 48;

        int dzien = (pesel.charAt(4) - 48) * 10;
        dzien += pesel.charAt(5) - 48;

        System.out.println("Rok: " + rok);
        System.out.println("Miesiąc: " + miesiac);
        System.out.println("Dzień: " + dzien);
    }

    /**
     * Wyświetla płeć na podstawie numeru PESEL
     * @param pesel numer PESEL
     */
    public static void wyswietlPlec(String pesel) {
        if(!sprawdzPoprawnosc(pesel)) {
            System.out.println("Podany PESEL nie jest poprawny");
            return;
        }
        int plec = pesel.charAt(9) - 48;
        System.out.println("Płeć: " + PLCIE[plec % 2]);
    }
}